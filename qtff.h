#ifndef QTFF_H__
#define QTFF_H__

#include <inttypes.h>

#include "atom.h"

extern Atom *qtff_load(int, size_t);
extern Atom *qtff_load_file(const char *, uint64_t);

#endif
