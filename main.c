#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>

#include "log.h"
#include "atom.h"
#include "qtff.h"


static void
usage(const char *progname)
{
	fprintf(stderr, "usage: %s [-o offset] [-v] <file>\n", progname);
}

int
main(int argc, char **argv)
{
	const char *progname;
	uint64_t offset;
	Atom *atom;
	int opt;

	progname = argv[0];
	offset = 0;
	while ((opt = getopt(argc, argv, "o:v")) != -1) {
		switch (opt) {
		case 'o':
			offset = strtoull(optarg, NULL, 0);
			break;
		case 'v':
			loglevelset(1);
			break;
		default:
			usage(progname);
			return EXIT_FAILURE;
		}
	}
	argv += optind;
	argc -= optind;

	if (argc < 1) {
		usage(progname);
		return EXIT_FAILURE;
	}

	atom = qtff_load_file(argv[0], offset);
	if (atom == NULL) {
		return EXIT_FAILURE;
	}

	atom_list_print(stdout, atom, 0);

	return EXIT_SUCCESS;
}
