#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "atom.h"


Atom *
atom_create(const char *type, size_t size)
{
	Atom *atom;

	atom = malloc(sizeof(Atom));
	if (atom == NULL) {
		return NULL;
	}

	memcpy(atom->type, type, sizeof(atom->type));
	atom->size = size;
	atom->next = NULL;
	atom->child = NULL;
	atom->info = NULL;
	atom->print = NULL;

	return atom;
}

void
atom_print(FILE *fp, const Atom *atom, int indent)
{
	fprintf(fp, "%*.*s", indent * 2, indent * 2, " ");
	fprintf(fp, "%-4.4s: %zu\n", atom->type, atom->size);
	if (atom->print != NULL) {
		(*atom->print)(fp, atom, indent);
	}
}

void
atom_list_print(FILE *fp, const Atom *list, int indent)
{
	const Atom *a;

	for (a = list; a != NULL; a = a->next) {
		atom_print(fp, a, indent);
		if (a->child != NULL)
			atom_list_print(fp, a->child, indent + 1);
	}
}
