#ifndef ATOM_H__
#define ATOM_H__

#include <stddef.h>
#include <stdio.h>

typedef struct Atom Atom;

struct Atom {
	Atom *next;
	size_t size;
	char type[4];
	Atom *child;
	void *info;
	void (*print)(FILE *, const Atom *, int);
};

extern Atom *atom_create(const char *, size_t);
extern void atom_print(FILE *, const Atom *, int);
extern void atom_list_print(FILE *, const Atom *, int);

#endif
