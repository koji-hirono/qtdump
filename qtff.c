#include <sys/stat.h>

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <fcntl.h>

#include "log.h"
#include "buf.h"
#include "atom.h"
#include "qtff.h"

typedef struct AtomCodec AtomCodec;

struct AtomCodec {
	const char *type;
	int (*decode)(int, Atom *);
};

void bytes_print(FILE *, const Atom *, int);
int bytes_decode(int, Atom *);

void
bytes_print(FILE *fp, const Atom *atom, int indent)
{
	size_t len = atom->size - 8;
	size_t i;
	unsigned char *buf = atom->info;

	(void)indent;

	fprintf(fp, "{\n");
	for (i = 0; i < len; i++) {
		fprintf(fp, " %02x", buf[i]);
		if ((i & 0xf) == 0xf)
			fprintf(fp, "\n");
	}
	fprintf(fp, "\n");
	fprintf(fp, "}\n");
}

int
bytes_decode(int fd, Atom *atom)
{
	size_t len = atom->size - 8;
	char *buf;
	int r;

	buf = malloc(len);
	if (buf == NULL)
		return -1;
	r = read(fd, buf, len);
	if (r < 0) {
		free(buf);
		return -1;
	}
	if (r != (int)len) {
		free(buf);
		return -1;
	}

	atom->info = buf;
	atom->print = bytes_print;

	return 0;
}

typedef struct MVHD MVHD;

struct MVHD {
	uint32_t ver_flags;
	uint32_t c_time;
	uint32_t m_time;
	uint32_t time_scale;
	uint32_t duration;
	uint32_t pre_rate;
	uint32_t pre_volume;
	char reserved1[10];
	uint32_t matrix[9];
	uint32_t preview_time;
	uint32_t preview_duration;
	uint32_t poster_time;
	uint32_t sel_time;
	uint32_t sel_duration;
	uint32_t curr_time;
	uint32_t next_track_id;
};

static void
mvhd_print(FILE *fp, const Atom *atom, int indent)
{
	const MVHD *h = atom->info;
	uint32_t version;
	uint32_t flags;
	struct tm *tm;
	time_t t;
	int i;

	version = (h->ver_flags >> 24) & 0xff;
	flags = h->ver_flags & 0xffffff;

	(void)indent;

	fprintf(fp, "{\n");
	fprintf(fp, "version(1)         : %"PRIu32"\n", version);
	fprintf(fp, "flags(3)           : %"PRIx32"\n", flags);
	fprintf(fp, "c_time(4)          : %"PRIu32"(", h->c_time);
	t = h->c_time - 2082844800;
	tm = gmtime(&t);
	fprintf(fp, "%d-%d-%d %02d:%02d:%02d)\n",
		tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday,
		tm->tm_hour, tm->tm_min, tm->tm_sec);
	fprintf(fp, "m_time(4)          : %"PRIu32"(", h->m_time);
	t = h->m_time - 2082844800;
	tm = gmtime(&t);
	fprintf(fp, "%d-%d-%d %02d:%02d:%02d)\n",
		tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday,
		tm->tm_hour, tm->tm_min, tm->tm_sec);
	fprintf(fp, "time_scale(4)      : %"PRIu32"\n", h->time_scale);
	fprintf(fp, "duration(4)        : %"PRIu32"\n", h->duration);

	fprintf(fp, "pre_rate(4)        : %"PRIu32"\n", h->pre_rate);
	fprintf(fp, "pre_volume(2)      : %"PRIu32"\n", h->pre_volume);

	fprintf(fp, "reserved(10)       :\n");
       	fprintf(fp, "  %02x %02x %02x %02x\n",
		h->reserved1[0], h->reserved1[1],
	       	h->reserved1[2], h->reserved1[3]);
       	fprintf(fp, "  %02x %02x %02x %02x\n",
		h->reserved1[4], h->reserved1[5],
	       	h->reserved1[6], h->reserved1[7]);
       	fprintf(fp, "  %02x %02x\n",
		h->reserved1[8], h->reserved1[9]);

	fprintf(fp, "matrix(36)          :\n");
	for (i = 0; i < 9; i++) {
		fprintf(fp, "  [%d] = %"PRIu32"\n", i, h->matrix[i]);
	}

	fprintf(fp, "preview time(4)     : %"PRIu32"\n", h->preview_time);
	fprintf(fp, "preview duration(4) : %"PRIu32"\n",
		h->preview_duration);

	fprintf(fp, "poster time(4)      : %"PRIu32"\n", h->poster_time);

	fprintf(fp, "sel time(4)         : %"PRIu32"\n", h->sel_time);
	fprintf(fp, "sel duration(4)     : %"PRIu32"\n",
		h->sel_duration);

	fprintf(fp, "current time(4)     : %"PRIu32"\n", h->curr_time);
	fprintf(fp, "next track id(4)    : %"PRIu32"\n", h->next_track_id);
	fprintf(fp, "}\n");
}

static int
mvhd_decode(int fd, Atom *atom)
{
	size_t len = atom->size - 8;
	MVHD *h;
	int i;
	Buf b;
	char *buf;
	int r;

	buf = malloc(len);
	if (buf == NULL)
		return -1;
	r = read(fd, buf, len);
	if (r < 0) {
		free(buf);
		return -1;
	}
	if (r != (int)len) {
		free(buf);
		return -1;
	}

	h = malloc(sizeof(*h));
	if (h == NULL) {
		free(buf);
		return -1;
	}

	buf_init(&b, buf, len);
	buf_read32(&b, &h->ver_flags);
	buf_read32(&b, &h->c_time);
	buf_read32(&b, &h->m_time);
	buf_read32(&b, &h->time_scale);
	buf_read32(&b, &h->duration);
	buf_read32(&b, &h->pre_rate);
	buf_read16(&b, &h->pre_volume);
	buf_readstr(&b, h->reserved1, sizeof(h->reserved1));
	for (i = 0; i < 9; i++) {
		buf_read32(&b, &h->matrix[i]);
	}
	buf_read32(&b, &h->preview_time);
	buf_read32(&b, &h->preview_duration);
	buf_read32(&b, &h->poster_time);
	buf_read32(&b, &h->sel_time);
	buf_read32(&b, &h->sel_duration);
	buf_read32(&b, &h->curr_time);
	buf_read32(&b, &h->next_track_id);

	free(buf);

	atom->info = h;
	atom->print = mvhd_print;

	return 0;
}

typedef struct TKHD TKHD;

struct TKHD {
	uint32_t ver_flags;
	uint32_t c_time;
	uint32_t m_time;
	uint32_t track_id;
	char reserved1[4];
	uint32_t duration;
	char reserved2[8];
	uint32_t layer;
	uint32_t alt_group;
	uint32_t volume;
	char reserved3[2];
	uint32_t matrix[9];
	uint32_t track_width;
	uint32_t track_height;
};

static void
tkhd_print(FILE *fp, const Atom *atom, int indent)
{
	const TKHD *h = atom->info;
	uint32_t version;
	uint32_t flags;
	struct tm *tm;
	time_t t;
	int i;

	version = (h->ver_flags >> 24) & 0xff;
	flags = h->ver_flags & 0xffffff;

	(void)indent;

	fprintf(fp, "{\n");
	fprintf(fp, "version(1)      : %"PRIu32"\n", version);
	fprintf(fp, "flags(3)        : %"PRIx32"\n", flags);
	fprintf(fp, "c_time(4)       : %"PRIu32"(", h->c_time);
	t = h->c_time - 2082844800;
	tm = gmtime(&t);
	fprintf(fp, "%d-%d-%d %02d:%02d:%02d)\n",
		tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday,
		tm->tm_hour, tm->tm_min, tm->tm_sec);
	fprintf(fp, "m_time(4)       : %"PRIu32"(", h->m_time);
	t = h->m_time - 2082844800;
	tm = gmtime(&t);
	fprintf(fp, "%d-%d-%d %02d:%02d:%02d)\n",
		tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday,
		tm->tm_hour, tm->tm_min, tm->tm_sec);
	fprintf(fp, "track_id(4)     : %"PRIu32"\n", h->track_id);
	fprintf(fp, "reserved(4)     :\n");
       	fprintf(fp, "  %02x %02x %02x %02x\n",
		h->reserved1[0], h->reserved1[1],
	       	h->reserved1[2], h->reserved1[3]);
	fprintf(fp, "duration(4)     : %"PRIu32"\n", h->duration);
	fprintf(fp, "reserved(8)     :\n");
       	fprintf(fp, "  %02x %02x %02x %02x\n",
		h->reserved2[0], h->reserved2[1],
	       	h->reserved2[2], h->reserved2[3]);
       	fprintf(fp, "  %02x %02x %02x %02x\n",
		h->reserved2[4], h->reserved2[5],
	       	h->reserved2[6], h->reserved2[7]);
	fprintf(fp, "layer(2)        : %"PRIu32"\n", h->layer);
	fprintf(fp, "alt group(2)    : %"PRIu32"\n", h->alt_group);
	fprintf(fp, "volume(2)       : %"PRIu32"\n", h->volume);
	fprintf(fp, "reserved(2)     : %02x %02x\n",
		h->reserved3[0], h->reserved3[1]);
	fprintf(fp, "matrix(36)      :\n");
	for (i = 0; i < 9; i++) {
		fprintf(fp, "  [%d] = %"PRIu32"\n", i, h->matrix[i]);
	}
	fprintf(fp, "track_width(4)  : %"PRIu32"\n", h->track_width);
	fprintf(fp, "track_height(4) : %"PRIu32"\n", h->track_height);
	fprintf(fp, "}\n");
}

static int
tkhd_decode(int fd, Atom *atom)
{
	size_t len = atom->size - 8;
	TKHD *h;
	int i;
	Buf b;
	char *buf;
	int r;

	buf = malloc(len);
	if (buf == NULL)
		return -1;
	r = read(fd, buf, len);
	if (r < 0) {
		free(buf);
		return -1;
	}
	if (r != (int)len) {
		free(buf);
		return -1;
	}

	h = malloc(sizeof(*h));
	if (h == NULL) {
		free(buf);
		return -1;
	}

	buf_init(&b, buf, len);
	buf_read32(&b, &h->ver_flags);
	buf_read32(&b, &h->c_time);
	buf_read32(&b, &h->m_time);
	buf_read32(&b, &h->track_id);
	buf_readstr(&b, h->reserved1, sizeof(h->reserved1));
	buf_read32(&b, &h->duration);
	buf_readstr(&b, h->reserved2, sizeof(h->reserved2));
	buf_read16(&b, &h->layer);
	buf_read16(&b, &h->alt_group);
	buf_read16(&b, &h->volume);
	buf_readstr(&b, h->reserved3, sizeof(h->reserved3));
	for (i = 0; i < 9; i++) {
		buf_read32(&b, &h->matrix[i]);
	}
	buf_read32(&b, &h->track_width);
	buf_read32(&b, &h->track_height);

	free(buf);

	atom->info = h;
	atom->print = tkhd_print;

	return 0;
}

typedef struct ELST ELST;
typedef struct ELST_elm ELST_elm;

struct ELST_elm {
	uint32_t track_duration;
	uint32_t media_time;
	uint32_t media_rate;
};

struct ELST {
	uint32_t flags;
	uint32_t n;
	ELST_elm *e;
};

static void
elst_print(FILE *fp, const Atom *atom, int indent)
{
	const ELST *h = atom->info;
	const ELST_elm *e;
	uint32_t i;

	(void)indent;

	fprintf(fp, "{\n");
	fprintf(fp, "flags(4): %"PRIu32"\n", h->flags);
	fprintf(fp, "n(4)    : %"PRIu32"\n", h->n);
	for (i = 0; i < h->n; i++) {
		fprintf(fp, "[%"PRIu32"] = {\n", i);
		e = &h->e[i];
		fprintf(fp, "  track_duration(4): %"PRIu32"\n",
			e->track_duration);
		fprintf(fp, "  media_time(4)    : %"PRIu32"\n",
			e->media_time);
		fprintf(fp, "  media_rate(4)    : %"PRIu32"\n",
			e->media_rate);
		fprintf(fp, "}\n");
	}
	fprintf(fp, "}\n");

}

static int
elst_decode(int fd, Atom *atom)
{
	size_t len = atom->size - 8;
	ELST *h;
	ELST_elm *e;
	uint32_t i;
	Buf b;
	char *buf;
	int r;

	buf = malloc(len);
	if (buf == NULL)
		return -1;
	r = read(fd, buf, len);
	if (r < 0) {
		free(buf);
		return -1;
	}
	if (r != (int)len) {
		free(buf);
		return -1;
	}

	h = malloc(sizeof(*h));
	if (h == NULL) {
		free(buf);
		return -1;
	}

	buf_init(&b, buf, len);
	buf_read32(&b, &h->flags);
	buf_read32(&b, &h->n);
	h->e = malloc(h->n * sizeof(*h->e));
	if (h->e == NULL) {
		free(h);
		free(buf);
		return -1;
	}
	for (i = 0; i < h->n; i++) {
		e = &h->e[i];
		buf_read32(&b, &e->track_duration);
		buf_read32(&b, &e->media_time);
		buf_read32(&b, &e->media_rate);
	}

	free(buf);

	atom->info = h;
	atom->print = elst_print;

	return 0;
}

typedef struct MDHD MDHD;

struct MDHD {
	uint32_t ver_flags;
	uint32_t c_time;
	uint32_t m_time;
	uint32_t time_scale;
	uint32_t duration;
	uint32_t lang;
	uint32_t quality;
};

static void
mdhd_print(FILE *fp, const Atom *atom, int indent)
{
	const MDHD *h = atom->info;
	uint32_t version;
	uint32_t flags;
	struct tm *tm;
	time_t t;

	version = (h->ver_flags >> 24) & 0xff;
	flags = h->ver_flags & 0xffffff;

	(void)indent;

	fprintf(fp, "{\n");
	fprintf(fp, "version(1)   : %"PRIu32"\n", version);
	fprintf(fp, "flags(3)     : %"PRIx32"\n", flags);
	fprintf(fp, "c_time(4)    : %"PRIu32"(", h->c_time);
	t = h->c_time - 2082844800;
	tm = gmtime(&t);
	fprintf(fp, "%d-%d-%d %02d:%02d:%02d)\n",
		tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday,
		tm->tm_hour, tm->tm_min, tm->tm_sec);
	fprintf(fp, "m_time(4)    : %"PRIu32"(", h->m_time);
	t = h->m_time - 2082844800;
	tm = gmtime(&t);
	fprintf(fp, "%d-%d-%d %02d:%02d:%02d)\n",
		tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday,
		tm->tm_hour, tm->tm_min, tm->tm_sec);
	fprintf(fp, "time_scale(4): %"PRIu32"\n", h->time_scale);
	fprintf(fp, "duration(4)  : %"PRIu32"\n", h->duration);
	fprintf(fp, "lang(2)      : %"PRIu32"\n", h->lang);
	fprintf(fp, "quality(2)   : %"PRIu32"\n", h->quality);
	fprintf(fp, "}\n");
}

static int
mdhd_decode(int fd, Atom *atom)
{
	size_t len = atom->size - 8;
	MDHD *h;
	Buf b;
	char *buf;
	int r;

	buf = malloc(len);
	if (buf == NULL)
		return -1;
	r = read(fd, buf, len);
	if (r < 0) {
		free(buf);
		return -1;
	}
	if (r != (int)len) {
		free(buf);
		return -1;
	}

	h = malloc(sizeof(*h));
	if (h == NULL) {
		free(buf);
		return -1;
	}

	buf_init(&b, buf, len);
	buf_read32(&b, &h->ver_flags);
	buf_read32(&b, &h->c_time);
	buf_read32(&b, &h->m_time);
	buf_read32(&b, &h->time_scale);
	buf_read32(&b, &h->duration);
	buf_read16(&b, &h->lang);
	buf_read16(&b, &h->quality);

	free(buf);

	atom->info = h;
	atom->print = mdhd_print;

	return 0;
}

typedef struct HDLR HDLR;

struct HDLR {
	uint32_t ver_flags;
	char type[4];
	char subtype[4];
	uint32_t manufacture;
	uint32_t flags;
	uint32_t flags_mask;
	char *name;
};

static void
hdlr_print(FILE *fp, const Atom *atom, int indent)
{
	const HDLR *h = atom->info;
	uint32_t version;
	uint32_t flags;

	version = (h->ver_flags >> 24) & 0xff;
	flags = h->ver_flags & 0xffffff;

	(void)indent;

	fprintf(fp, "{\n");
	fprintf(fp, "version(1)      : %"PRIu32"\n", version);
	fprintf(fp, "flags(3)        : %"PRIx32"\n", flags);
	fprintf(fp, "c type(4)       : %-4.4s\n", h->type);
	fprintf(fp, "c subtype(4)    : %-4.4s\n", h->subtype);
	fprintf(fp, "c manufacture(4): %"PRIu32"\n", h->manufacture);
	fprintf(fp, "c flags(4)      : %"PRIx32"\n", h->flags);
	fprintf(fp, "c flags_mask(4) : %"PRIx32"\n", h->flags_mask);
	fprintf(fp, "c name(%zu)       : %s\n",
		strlen(h->name) + 1, h->name);
	fprintf(fp, "}\n");
}

static int
hdlr_decode(int fd, Atom *atom)
{
	size_t len = atom->size - 8;
	HDLR *h;
	Buf b;
	char *buf;
	int r;

	buf = malloc(len);
	if (buf == NULL)
		return -1;
	r = read(fd, buf, len);
	if (r < 0) {
		free(buf);
		return -1;
	}
	if (r != (int)len) {
		free(buf);
		return -1;
	}

	h = malloc(sizeof(*h));
	if (h == NULL) {
		free(buf);
		return -1;
	}

	buf_init(&b, buf, len);
	buf_read32(&b, &h->ver_flags);
	buf_readstr(&b, h->type, sizeof(h->type));
	buf_readstr(&b, h->subtype, sizeof(h->type));
	buf_read32(&b, &h->manufacture);
	buf_read32(&b, &h->flags);
	buf_read32(&b, &h->flags_mask);
	if (b.len == 1) {
		h->name = "";
	} else {
		h->name = strdup(b.data);
		if (h->name == NULL) {
			free(h);
			free(buf);
			return -1;
		}
	}

	free(buf);

	atom->info = h;
	atom->print = hdlr_print;

	return 0;
}

static int
vmhd_decode(int fd, Atom *atom)
{
	return bytes_decode(fd, atom);
}

static int
dref_decode(int fd, Atom *atom)
{
	return bytes_decode(fd, atom);
}

const AtomCodec codec_list[] = {
	{"mvhd", mvhd_decode},
	{"tkhd", tkhd_decode},
	{"elst", elst_decode},
	{"mdhd", mdhd_decode},
	{"hdlr", hdlr_decode},
	{"vmhd", vmhd_decode},
	{"dref", dref_decode},
	{NULL, NULL}
};

static int
atom_decode(int fd, Atom *atom)
{
	const AtomCodec *c;

	for (c = codec_list; c->type != NULL; c++) {
		if (memcmp(atom->type, c->type, sizeof(atom->type)) == 0) {
			return (*c->decode)(fd, atom);
		}
	}

	if (lseek(fd, atom->size - 8, SEEK_CUR) < 0) {
		E("seek failed.");
		return -1;
	}

	return 0;
}

static int
has_child(Atom *atom)
{
	const char *list[] = {
		"moov",
		"trak",
		"edts",
		"mdia",
		"minf",
		"stbl",
		"dinf",
		"udta",
		"NCDT"
	};
	size_t n = sizeof(list) / sizeof(list[0]);
	size_t i;

	for (i = 0; i < n; i++) {
		if (memcmp(atom->type, list[i], sizeof(atom->type)) == 0)
			return 1;
	}

	return 0;
}

Atom *
qtff_load(int fd, size_t max)
{
	Atom *head;
	Atom *atom;
	Atom **prev;
	char buf[8];
	char type[4];
	uint32_t size;
	size_t len;
	Buf b;
	int r;

	D("start max:%zu", max);

	len = 0;

	head = NULL;
	prev = &head;
	while (len < max) {
		r = read(fd, buf, sizeof(buf));
		if (r < 0) {
			E("read failed.");
			break;
		}
		if (r != 8) {
			E("too few len: %d (%zu/%zu)", r, len, max);
			break;
		}

		buf_init(&b, buf, r);
		buf_read32(&b, &size);
		buf_readstr(&b, type, sizeof(type));

		D("type: %-4.4s", type);
		D("size: %"PRIu32, size);

		atom = atom_create(type, size);
		if (atom == NULL) {
			E("atom_create failed.");
			break;
		}

		*prev = atom;
		prev = &atom->next;

		if (has_child(atom)) {
			atom->child = qtff_load(fd, atom->size - 8);
		} else {
			atom_decode(fd, atom);
		}
		len += atom->size;
	}

	D("end");

	return head;
}

Atom *
qtff_load_file(const char *fname, uint64_t offset)
{
	struct stat st;
	size_t max;
	Atom *list;
	int fd;

	fd = open(fname, O_RDONLY);
	if (fd == -1)
		return NULL;

	if (fstat(fd, &st) < 0) {
		E("fstat failed.");
		return NULL;
	}

	if (offset != 0) {
		if (lseek(fd, offset, SEEK_SET) < 0) {
			E("seek failed.");
			return NULL;
		}
	}

	max = st.st_size - offset;

	list = qtff_load(fd, max);

	close(fd);
	return list;
}
