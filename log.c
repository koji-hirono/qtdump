#include <stdio.h>
#include <stdarg.h>

static int g_lvl = 2;

static const char *
strlevel(int lvl)
{
	switch (lvl) {
	case 1:
		return "D";
	case 2:
		return "W";
	case 3:
		return "E";
	default:
		return "";
	}
}

static void
logvprint(int lvl, const char *func, int line, const char *fmt, va_list ap)
{
	printf("%s <%s:%d> ", strlevel(lvl), func, line);
	vprintf(fmt, ap);
	printf("\n");
}

void
logprint(int lvl, const char *func, int line, const char *fmt, ...)
{
	va_list ap;

	if (lvl < g_lvl)
		return;

	va_start(ap, fmt);
	logvprint(lvl, func, line, fmt, ap);
	va_end(ap);
}

void
loglevelset(int lvl)
{
	g_lvl = lvl;
}
