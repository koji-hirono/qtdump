#ifndef LOG_H__
#define LOG_H__

#define _logprint(lvl, ...) logprint(lvl, __func__, __LINE__, __VA_ARGS__)

#define D(...) _logprint(1, __VA_ARGS__)
#define W(...) _logprint(2, __VA_ARGS__)
#define E(...) _logprint(3, __VA_ARGS__)

extern void logprint(int, const char *, int, const char *, ...);
extern void loglevelset(int);

#endif
