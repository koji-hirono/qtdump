PROG=qtdump
SRCS=\
     main.c \
     qtff.c \
     atom.c \
     buf.c \
     log.c

OBJS=$(SRCS:.c=.o)

CC=clang
CFLAGS=-Wall -W -Werror

all: $(PROG)

clean:
	rm -f $(OBJS) $(PROG) *~

$(PROG): $(OBJS)
	$(CC) $(CFLAGS) -o $@ $(OBJS) $(LDADD)

%.o : %.c
	$(CC) -c $(CFLAGS) -o $@ $<
