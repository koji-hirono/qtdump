#ifndef BUF_H__
#define BUF_H__

#include <inttypes.h>
#include <stddef.h>

typedef struct Buf Buf;

struct Buf {
	size_t len;
	char *data;
};

extern void buf_init(Buf *, void *, size_t);
extern int buf_read8(Buf *, uint32_t *);
extern int buf_read16(Buf *, uint32_t *);
extern int buf_read24(Buf *, uint32_t *);
extern int buf_read32(Buf *, uint32_t *);
extern int buf_readstr(Buf *, char *, size_t);

#endif
