#include <inttypes.h>
#include <stddef.h>

#include "buf.h"


void
buf_init(Buf *b, void *data, size_t len)
{
	b->len = len;
	b->data = data;
}

int
buf_read8(Buf *b, uint32_t *v)
{
	if (b->len < 1)
		return -1;
	*v = b->data[0] & 0xff;
	b->data += 1;
	b->len -= 1;
	return 0;
}

int
buf_read16(Buf *b, uint32_t *v)
{
	if (b->len < 2)
		return -1;
	*v = b->data[0] & 0xff;
	*v <<= 8;
	*v |= b->data[1] & 0xff;
	b->data += 2;
	b->len -= 2;
	return 0;
}

int
buf_read24(Buf *b, uint32_t *v)
{
	if (b->len < 3)
		return -1;
	*v = b->data[0] & 0xff;
	*v <<= 8;
	*v |= b->data[1] & 0xff;
	*v <<= 8;
	*v |= b->data[2] & 0xff;
	b->data += 3;
	b->len -= 3;
	return 0;
}

int
buf_read32(Buf *b, uint32_t *v)
{
	if (b->len < 4)
		return -1;
	*v = b->data[0] & 0xff;
	*v <<= 8;
	*v |= b->data[1] & 0xff;
	*v <<= 8;
	*v |= b->data[2] & 0xff;
	*v <<= 8;
	*v |= b->data[3] & 0xff;
	b->data += 4;
	b->len -= 4;
	return 0;
}

int
buf_readstr(Buf *b, char *v, size_t len)
{
	size_t i;

	if (b->len < len)
		return -1;
	for (i = 0; i < len; i++)
		v[i] = b->data[i];
	b->data += len;
	b->len -= len;
	return 0;
}
